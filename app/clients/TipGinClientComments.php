<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 16.11.15.
 * Time: 11:26
 * Client for http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml
 * Stores the xml in the soccer folder,parses and stores in the database.
 */

namespace App\clients;


use App\Commentarie;
use App\Fixture;
use App\LeagueFixture;
use App\MatchFixture;
use App\parser\FixturesParser;

use App\parser\SimpleFixturesParser;
use App\parser\SoccerCommentsParser;
use App\SimpleLeagueFixture;
use App\SimpleMatchFixture;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class TipGinClientComments
{


    public $feed_name;
    public $dir = '/soccer_comments';

    public $country_arr = array('afc_champleague', 'africa_nations_cup', 'argentina', 'argentina_b', 'asia_cup', 'australia', 'austria', 'belgium', 'bolivia', 'brazil_a', 'brazil_b', 'championship', 'champleague', 'chile', 'club_wc', 'colombia', 'concacaf_champleague', 'concacaf_gold_cup', 'concacaf_superliga', 'confiderationscup', 'copaamerica', 'denmark', 'ecuador', 'eng_league_carling', 'eng_league_charity', 'eng_league_conference', 'eng_league_one', 'eng_league_two', 'epl', 'holland', 'poland', 'euro', 'eredivisie', 'europaleague', 'euroqualifying', 'facup', 'france', 'france_cup', 'france_league2', 'france_league_cup', 'germany', 'germany_cup', 'germany_liga2', 'greece', 'holland_cup', 'holland_division2', 'ireland', 'italy', 'italy_cup', 'italy_serieb', 'japan', 'libertadores', 'mexico', 'mls', 'n_ireland', 'olympic', 'paraguay', 'peru', 'portugal', 'rsa', 'russia', 'scotland', 'scotland_cup', 'scotland_division2', 'scotland_league_cup', 'spain', 'spain_cup', 'spain_segunda', 'spain_supercup', 'uefa_supercup', 'sudamericana', 'sweden', 'switzerland', 'turkey', 'uruguay', 'venezuela', 'wales', 'worldcupqualifying');

    public function startJob()
    {

        //$this->testLocal();

        $this->dropData();
        foreach ($this->country_arr as $country) {
            if ($this->checkDirectory($country)) {


                if ($this->downloadFeed($country)) {


                    $this->parseFeed($country);


                }

            } else {


                Log::error(get_class($this), ['context' => 'Failed at fetching Comment Feed.']);
            }


        }
    }

    private function checkDirectory($country)
    {
        try {
            if (!file_exists(base_path() . $this->dir)) {

                $result = File::makeDirectory(base_path() . $this->dir, 0775);

            }

            if (file_exists(base_path() . $this->dir . '/' . $country . '.xml')) {

                File::delete(base_path() . $this->dir . '/' . $country . '.xml');

            }


            return true;
        } catch (Exception $e) {
            Log::error(get_class($this), ['context' => 'Failed at checkDirectory exception:' . $e]);
            return false;
        }
    }

    private function downloadFeed($country)
    {


        $myFile = fopen(base_path() . $this->dir . '/' . $country . '.xml', "w");
        $client = new Client();
        $account_name = env('ACCOUNT_NAME');
        $request = $client->get('http://www.tipgin.net/datav2/accounts/' . $account_name . '/soccer/commentaries/' . $country . '.xml', ['save_to' => $myFile]);

        if ($request->getStatusCode() == '200') {


            return true;
        } else {
            Log::error(get_class($this), ['context' => 'Failed downloading feed HTTP code:' . $request->getStatusCode()]);
            return false;
        }

    }


    private function parseFeed($country)
    {

        try {
            $xml = simplexml_load_file(base_path() . $this->dir . '/' . $country . '.xml');
            $parser = new SoccerCommentsParser();

            $parser->startParser($xml, $country);
        } catch (Exception $e) {

            Log::error(get_class($this), ['context' => 'Failed SimpleXML parser:' . $e]);

        }

    }

    /*
     * Truncate the tables and reset row id's
     * */

    private function dropData()
    {

        Commentarie::truncate();


    }

    private function testLocal()
    {

        /*$this->dropData();
        foreach ($this->country_arr as $country) {


            $this->parseFeed($country);
        }*/
    }

}