<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 16.11.15.
 * Time: 11:26
 * Client for http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml
 * Stores the xml in the soccer folder,parses and stores in the database.
 */

namespace App\clients;


use App\parser\SoccerOddsParser;
use App\SoccerMatchOds;
use App\SoccerMatchOdsDetails;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class TipGinClientOdds
{


    public $feed_name;
    public $dir = '/soccer_odds';

    public $country_arr = array('africa', 'albania', 'algeria', 'argentina', 'austria', 'algeria', 'angola', 'armenia', 'asia', 'australia', 'azerbaijan', 'belarus', 'belgium', 'bolivia', 'bosnia', 'brazil', 'bulgaria', 'cameroon', 'canada', 'chile', 'china', 'colombia', 'concacaf', 'congo', 'croatia', 'cyprus', 'czech', 'costarica', 'denmark', 'equador', 'egypt', 'elsalvador', 'england', 'estonia', 'europe', 'finland', 'france', 'georgia', 'germany', 'ghana', 'greece', 'guatemala', 'holland', 'honduras', 'hungary', 'iceland', 'india', 'indonesia', 'international', 'iran', 'ireland', 'israel', 'italy', 'japan', 'jordan', 'kazakhstan', 'kenya', 'korea', 'kuwait', 'latvia', 'lithuania', 'macedonia', 'malaysia', 'malta', 'mexico', 'moldova', 'montenegro', 'morocco', 'newzealand', 'nigeria', 'norway', 'oceania', 'paraguay', 'peru', 'poland', 'portugal', 'qatar', 'romania', 'russia', 'saudiarabia', 'scotland', 'serbia', 'singapore', 'slovakia', 'slovenia', 'southafrica', 'southamerica', 'spain', 'sweden', 'switzerland', 'thailand', 'tunisia', 'turkey', 'uae', 'usa', 'ukraine', 'uruguay', 'uzbekistan', 'venezuela', 'vietnam', 'wales', 'worldcup');

    public function startJob()
    {

        //$this->testLocal();

        $this->dropData();
        foreach ($this->country_arr as $country) {
            if ($this->checkDirectory($country)) {


                if ($this->downloadFeed($country)) {


                    $this->parseFeed($country);


                }

            } else {


                Log::error(get_class($this), ['context' => 'Failed at fetching Odds Feed.']);
            }


        }
    }

    private function checkDirectory($country)
    {
        try {
            if (!file_exists(base_path() . $this->dir)) {

                $result = File::makeDirectory(base_path() . $this->dir, 0775);

            }

            if (file_exists(base_path() . $this->dir . '/' . $country . '.xml')) {

                File::delete(base_path() . $this->dir . '/' . $country . '.xml');

            }


            return true;
        } catch (Exception $e) {
            Log::error(get_class($this), ['context' => 'Failed at checkDirectory exception:' . $e]);
            return false;
        }
    }

    private function downloadFeed($country)
    {


        $myFile = fopen(base_path() . $this->dir . '/' . $country . '.xml', "w");
        $client = new Client();
        $account_name = env('ACCOUNT_NAME');
        $request = $client->get('http://www.tipgin.net/datav2/accounts/' . $account_name . '/soccer/odds/' . $country . '.xml', ['save_to' => $myFile]);

        if ($request->getStatusCode() == '200') {


            return true;
        } else {
            Log::error(get_class($this), ['context' => 'Failed downloading feed HTTP code:' . $request->getStatusCode()]);
            return false;
        }

    }


    private function parseFeed($country)
    {

        try {
            $xml = simplexml_load_file(base_path() . $this->dir . '/' . $country . '.xml');



            $parser = new SoccerOddsParser();

            $parser->startParser($xml);
        } catch (Exception $e) {

            Log::error(get_class($this), ['context' => 'Failed SimpleXML parser:' . $e]);

        }

    }

    /*
     * Truncate the tables and reset row id's
     * */

    private function dropData()
    {

        SoccerMatchOds::truncate();
        SoccerMatchOdsDetails::truncate();



    }

    private function testLocal()
    {

        $this->dropData();
        foreach ($this->country_arr as $country) {


            $this->parseFeed($country);
        }
    }

}