<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 16.11.15.
 * Time: 11:26
 * Client for http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml
 * Stores the xml in the soccer folder,parses and stores in the database.
 */

namespace App\clients;


use App\parser\SoccerHighlightsParser;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class TipGinClientHighlight
{


    public $feed_name;
    public $dir = '/soccer_highlights';
    public $high_file='d-1';


    public function startJob()
    {

        //$this->testLocal();



            if ($this->checkDirectory()) {


                if ($this->downloadFeed()) {


                    $this->parseFeed();


                }

            } else {


                Log::error(get_class($this), ['context' => 'Failed at fetching Results Feed.']);
            }



    }

    private function checkDirectory()
    {
        try {
            if (!file_exists(base_path() . $this->dir)) {

                $result = File::makeDirectory(base_path() . $this->dir, 0775);

            }

            if (file_exists(base_path() . $this->dir . '/' . $this->high_file. '.xml')) {

                File::delete(base_path() . $this->dir . '/' . $this->high_file . '.xml');

            }


            return true;
        } catch (Exception $e) {
            Log::error(get_class($this), ['context' => 'Failed at checkDirectory exception:' . $e]);
            return false;
        }
    }

    private function downloadFeed()
    {


        $myFile = fopen(base_path() . $this->dir . '/' . $this->high_file. '.xml', "w");
        $client = new Client();
        $account_name = env('ACCOUNT_NAME');
        $request = $client->get('http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/highlights/d-1.xml', ['save_to' => $myFile]);

        if ($request->getStatusCode() == '200') {


            return true;
        } else {
            Log::error(get_class($this), ['context' => 'Failed downloading feed HTTP code:' . $request->getStatusCode()]);
            return false;
        }

    }


    private function parseFeed()
    {

        try {
            $xml = simplexml_load_file(base_path() . $this->dir . '/' . $this->high_file. '.xml');



            $parser = new SoccerHighlightsParser();

            $parser->startParser($xml);
        } catch (Exception $e) {

            Log::error(get_class($this), ['context' => 'Failed SimpleXML parser:' . $e]);

        }

    }

    /*
     * Truncate the tables and reset row id's
     * */



    private function testLocal()
    {





            $this->parseFeed();

    }

}