<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 16.11.15.
 * Time: 11:26
 * Client for http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml
 * Stores the xml in the soccer folder,parses and stores in the database.
 */

namespace App\clients;


use App\parser\SoccerResultsParser;
use App\ResultLeague;
use App\ResultMatch;
use App\ResultMatchAwayPlayer;
use App\ResultMatchAwayPlayerSubs;
use App\ResultMatchHomePlayer;
use App\ResultMatchHomePlayerSubs;
use App\ResultStage;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class TipGinClientResults
{


    public $feed_name;
    public $dir = '/soccer_results';

    public $country_arr = array('albania', 'algeria', 'andorra', 'angola', 'argentina', 'armenia', 'aruba', 'australia', 'austria', 'azerbaijan', 'bahrain', 'bangladesh', 'barbados', 'belarus', 'belgium', 'belize', 'bermuda', 'bhutan', 'bolivia', 'bosnia', 'botswana', 'brazil', 'brunei', 'bulgaria', 'cambodia', 'cameroon', 'canada', 'chile', 'china', 'chinesetaipei', 'colombia', 'costarica', 'croatia', 'cyprus', 'czech', 'denmark', 'ecuador', 'egypt', 'elsalvador', 'england', 'estonia', 'europe', 'faroeislands', 'fiji', 'finland', 'france', 'friendly', 'gabon', 'georgia', 'germany', 'ghana', 'greece', 'grenada', 'guadeloupe', 'guatemala', 'haiti', 'holland', 'honduras', 'hongkong', 'hungary', 'iceland', 'india', 'indonesia', 'international', 'iran', 'iraq', 'ireland', 'israel', 'italy', 'ivorycoast', 'jamaica', 'japan', 'jordan', 'kazakhstan', 'korea', 'kuwait', 'latvia', 'lebanon', 'libya', 'lithuania', 'luxembourg', 'macedonia', 'malaysia', 'malta', 'mexico', 'moldova', 'montenegro', 'morocco', 'namibia', 'nepal', 'newzealand', 'nicaragua', 'nigeria', 'northernireland', 'norway', 'oman', 'pakistan', 'panama', 'paraguay', 'peru', 'poland', 'portugal', 'qatar', 'romania', 'russia', 'sanmarino', 'saudiarabia', 'scotland', 'senegal', 'serbia', 'singapore', 'slovakia', 'slovenia', 'southafrica', 'spain', 'sudan', 'surinam', 'sweden', 'switzerland', 'syria', 'thailand', 'trinidadandtobago', 'tunisia', 'turkey', 'uae', 'ukraine', 'uruguay', 'usa', 'uzbekistan', 'venezuela', 'vietnam', 'wales', 'worldcup', 'yemen');

    public function startJob()
    {

        //$this->testLocal();

        $this->dropData();
        foreach ($this->country_arr as $country) {
            if ($this->checkDirectory($country)) {


                if ($this->downloadFeed($country)) {


                    $this->parseFeed($country);


                }

            } else {


                Log::error(get_class($this), ['context' => 'Failed at fetching Results Feed.']);
            }


        }
    }

    private function checkDirectory($country)
    {
        try {
            if (!file_exists(base_path() . $this->dir)) {

                $result = File::makeDirectory(base_path() . $this->dir, 0775);

            }

            if (file_exists(base_path() . $this->dir . '/' . $country . '.xml')) {

                File::delete(base_path() . $this->dir . '/' . $country . '.xml');

            }


            return true;
        } catch (Exception $e) {
            Log::error(get_class($this), ['context' => 'Failed at checkDirectory exception:' . $e]);
            return false;
        }
    }

    private function downloadFeed($country)
    {


        $myFile = fopen(base_path() . $this->dir . '/' . $country . '.xml', "w");
        $client = new Client();
        $account_name = env('ACCOUNT_NAME');
        $request = $client->get('http://www.tipgin.net/datav2/accounts/' . $account_name . '/soccer/results/' . $country . '.xml', ['save_to' => $myFile]);

        if ($request->getStatusCode() == '200') {


            return true;
        } else {
            Log::error(get_class($this), ['context' => 'Failed downloading feed HTTP code:' . $request->getStatusCode()]);
            return false;
        }

    }


    private function parseFeed($country)
    {

        try {
            $xml = simplexml_load_file(base_path() . $this->dir . '/' . $country . '.xml');



            $parser = new SoccerResultsParser();

            $parser->startParser($xml, $country);
        } catch (Exception $e) {

            Log::error(get_class($this), ['context' => 'Failed SimpleXML parser:' . $e]);

        }

    }

    /*
     * Truncate the tables and reset row id's
     * */

    private function dropData()
    {

        ResultLeague::truncate();
        ResultStage::truncate();
        ResultMatch::truncate();
        ResultMatchAwayPlayer::truncate();
        ResultMatchHomePlayer::truncate();
        ResultMatchAwayPlayerSubs::truncate();
        ResultMatchHomePlayerSubs::truncate();



    }

    private function testLocal()
    {

        $this->dropData();
        foreach ($this->country_arr as $country) {


            $this->parseFeed($country);
        }
    }

}