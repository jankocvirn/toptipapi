<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 16.11.15.
 * Time: 11:26
 * Client for http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml
 * Stores the xml in the soccer folder,parses and stores in the database.
 */

namespace App\clients;


use App\parser\SoccerParser;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class TipGinClient
{



    public $feed_name;



    public function startJob(){

        if ($this->checkDirectory()){


            if ($this->downloadFeed()) {


               $this->parseFeed();


            }


        }

        else{


            Log::error('SoccerBasicLivescore', ['context' => 'Failed at fetching Livescore Feed.']);
        }



    }

    private function checkDirectory()
    {
        try{
        if (!file_exists(base_path() . '/soccer')) {

            $result = File::makeDirectory(base_path() . '/soccer', 0775);

        }

        if (file_exists(base_path() . '/soccer/livescore.xml')) {

            File::delete(base_path() . '/soccer/livescore.xml');

        }



        return true;
    }
        catch (Exception $e){
            Log::error('SoccerBasicLivescore', ['context' => 'Failed at checkDirectory exception:'.$e]);
        return false;
        }
    }

    private function downloadFeed(){


        $myFile = fopen(base_path().'/soccer/livescore.xml', "w");
        $client = new Client();
        $account_name=env('ACCOUNT_NAME');
        $request = $client->get('http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml', ['save_to' => $myFile]);

        if ($request->getStatusCode()=='200'){


            return true;
        }

        else{
            Log::error('SoccerBasicLivescore', ['context' => 'Failed downloading feed HTTP code:'.$request->getStatusCode()]);
            return false;
        }

    }



    private function parseFeed(){

        $xml = simplexml_load_file(base_path() . '/soccer/livescore.xml');
        $parser=new SoccerParser();

        $parser->parseXMLFeed($xml);

    }

}