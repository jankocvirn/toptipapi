<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 16.11.15.
 * Time: 11:26
 * Client for http://www.tipgin.net/datav2/accounts/'.$account_name.'/soccer/livescore/livescore.xml
 * Stores the xml in the soccer folder,parses and stores in the database.
 */

namespace App\clients;


use App\parser\SoccerScoresParser;
use App\SoccerScore;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;


class TipGinClientScores
{


    public $feed_name;
    public $dir = '/soccer_scores';

    public $country_arr = array('albania', 'algeria', 'argentina', 'armenia', 'australia', 'austria', 'azerbaijan', 'belarus', 'belgium', 'bosnia', 'brazil', 'bulgaria', 'chile', 'china', 'colombia', 'costarica', 'croatia', 'cyprus', 'czech', 'denmark', 'ecuador', 'egypt', 'england', 'estonia', 'finland', 'france', 'georgia', 'germany', 'greece', 'holland', 'hungary', 'iceland', 'ireland', 'israel', 'italy', 'japan', 'kazakhstan', 'korea', 'kuwait', 'latvia', 'lithuania', 'macedonia', 'mexico', 'moldova', 'morocco', 'norway', 'paraguay', 'peru', 'poland', 'qatar', 'romania', 'russia', 'saudiarabia', 'scotland', 'serbia', 'slovakia', 'slovenia', 'spain', 'sweden', 'switzerland', 'turkey', 'uae', 'ukraine', 'uruguay', 'usa' );
    public function startJob()
    {

        //$this->testLocal();

        $this->dropData();
        foreach ($this->country_arr as $country) {
            if ($this->checkDirectory($country)) {


                if ($this->downloadFeed($country)) {


                    $this->parseFeed($country);


                }

            } else {


                Log::error(get_class($this), ['context' => 'Failed at fetching Scores Feed.']);
            }


        }
    }

    private function checkDirectory($country)
    {
        try {
            if (!file_exists(base_path() . $this->dir)) {

                $result = File::makeDirectory(base_path() . $this->dir, 0775);

            }

            if (file_exists(base_path() . $this->dir . '/' . $country . '.xml')) {

                File::delete(base_path() . $this->dir . '/' . $country . '.xml');

            }


            return true;
        } catch (Exception $e) {
            Log::error(get_class($this), ['context' => 'Failed at checkDirectory exception:' . $e]);
            return false;
        }
    }

    private function downloadFeed($country)
    {


        $myFile = fopen(base_path() . $this->dir . '/' . $country . '.xml', "w");
        $client = new Client();
        $account_name = env('ACCOUNT_NAME');
        $request = $client->get('http://www.tipgin.net/datav2/accounts/' . $account_name . '/soccer/scorers/' . $country . '.xml', ['save_to' => $myFile]);

        if ($request->getStatusCode() == '200') {


            return true;
        } else {
            Log::error(get_class($this), ['context' => 'Failed downloading feed HTTP code:' . $request->getStatusCode()]);
            return false;
        }

    }


    private function parseFeed($country)
    {

        try {
            $xml = simplexml_load_file(base_path() . $this->dir . '/' . $country . '.xml');



            $parser = new SoccerScoresParser();

            $parser->startParser($xml, $country);
        } catch (Exception $e) {

            Log::error(get_class($this), ['context' => 'Failed SimpleXML parser:' . $e]);

        }

    }

    /*
     * Truncate the tables and reset row id's
     * */

    private function dropData()
    {

        SoccerScore::truncate();



    }

    private function testLocal()
    {

        $this->dropData();
        foreach ($this->country_arr as $country) {


            $this->parseFeed($country);
        }
    }

}