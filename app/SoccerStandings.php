<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerStandings extends Model
{
    protected $fillable = [

        'updated',
        'sport',
        'country',
        'league_country',
        'league_id',
        'league_sub_id',
        'league_name',
        'league_season',
        'league_round',
        'team_id',
        'team_name',
        'team_position',
        'recent_form',
        'status',

        'overall_draw',
        'overall_goal_attempted',
        'overall_scored',
        'overall_lose',
        'overall_played',
        'overall_win',

        'home_draw',
        'home_goal_attempted',
        'home_scored',
        'home_lose',
        'home_played',
        'home_win',

        'away_draw',
        'away_goal_attempted',
        'away_scored',
        'away_lose',
        'away_played',
        'away_win',

        'totals_goal_diff',
        'totals_points',
        'special_name'

    ];
}
