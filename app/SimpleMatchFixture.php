<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimpleMatchFixture extends Model
{
    protected $fillable=[

        'status','time','static_id','date',
        'alternate_id','alternate_id_2','matchid','league_id','league_sub_id',
        'homename','homeid','awayname','awayid'

    ];
}
