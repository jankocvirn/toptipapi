<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultLeague extends Model
{
    protected $fillable=[

        'league_id',
        'name',
        'season',
        'sub_id',
        'country',
        'uploaded',
        'sport'
        
        
    ];
}
