<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentarie extends Model
{
    protected $fillable = [

        'sport',
        'country',
        'league_name',
        'league_id',
        'league_sub_id',
        'alternate_id',
        'alternate_id_2',
        'date',
        'match_id',
        'static_id',
        'status',
        'time',
        'home_goals',
        'home_id',
        'home_name',
        'away_goals',
        'away_id',
        'away_name',
        'stadium',
        'attendance_name',
        'time_name',
        'referee',
        'comment',
        'comment_id',
        'comment_important',
        'comment_isgoal',
        'comment_minute'
    ];

    public function setCommentAttribute($value){

        if ($value==null){
            $this->attributes['comment']='none';
        }
        else{
            $this->attributes['comment']=$value;
        }
    }
}
