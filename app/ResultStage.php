<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultStage extends Model
{

    protected $fillable=[

        'league_id',
        'name',
        'round',
        'stage_id'


    ];
    
}
