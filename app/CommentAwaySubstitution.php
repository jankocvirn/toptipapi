<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentAwaySubstitution extends Model
{
    protected $fillable = [

        'match_id',
        'match_static_id',
        'minute',
        'off',
        'off_id',
        'on',
        'on_id'


    ];
}
