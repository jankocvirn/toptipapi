<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerMatchOdsDetails extends Model
{
    protected $fillable = [

        'match_id',
        'type_name',
        'bookmark_id',
        'bookmark_name',
        'odd_name',
        'odd_value'


    ];
}
