<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultMatchHomePlayerSubs extends Model
{
    protected $fillable = [

        'match_id',
        'league_id',
        'league_sub_id',
        'match_static_id',
        'minute',
        'player_in_booking',
        'player_in_name',
        'player_in_id',
        'player_in_number',
        'player_out_id',
        'player_out_name'


    ];
}
