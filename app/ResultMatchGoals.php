<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultMatchGoals extends Model
{
    protected $fillable = [

        'match_id',
        'league_id',
        'league_sub_id',
        'match_static_id',
        'assist',
        'minute',
        'player',
        'playerid',
        'score',
        'team'


    ];
}
