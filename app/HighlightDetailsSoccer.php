<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighlightDetailsSoccer extends Model
{
    protected $fillable = [

        'match_id',
        'league_id',
        'link'


    ];
}
