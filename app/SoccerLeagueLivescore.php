<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerLeagueLivescore extends Model
{

    protected $table='soccer_league_livescores';

    protected $fillable=[

        'id','country',
        'name',
        'cup',
        'sub_id',
        'soccer_leaguecol',
        'updated'

    ];



}
