<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentStatistics extends Model
{
    protected $fillable = [


        'match_id',
        'match_static_id',

        'home_shots_ongoal',
        'home_shots_total',
        'home_fouls_total',
        'home_corners_total',
        'home_offsides_total',
        'home_poss_time_total',
        'home_yc_total',
        'home_rc_total',
        'home_saves_total',

        'away_shots_ongoal',
        'away_shots_total',
        'away_fouls_total',
        'away_corners_total',
        'away_offsides_total',
        'away_poss_time_total',
        'away_yc_total',
        'away_rc_total',
        'away_saves_total'


    ];
}
