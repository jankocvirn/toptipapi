<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultMatchHomePlayer extends Model
{
    protected $fillable = [

        'match_id',
        'league_id',
        'league_sub_id',
        'match_static_id',
        'booking',
        'player_id',
        'name',
        'number'


    ];
}
