<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultMatch extends Model
{
    protected $fillable = [


        'stage_id',
        'league_id',
        'alternate_id',
        'alternate_id_2',
        'date',
        'match_id',
        'static_id',
        'status',
        'time',
        'venue',
        'venue-city',
        'venue_id',
        'het_score',
        'hft_score',
        'home_id',
        'home_name',
        'home_pen_score',
        'home_score',
        'aet_score',
        'aft_score',
        'away_id',
        'away_name',
        'away_pen_score',
        'away_score',
        'half_time_score'


    ];
}
