<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerLivescoreMatch extends Model
{
    protected $fillable=[

        'league_parent_id',
        'alternate_id',
        'alternate_id_2',
        'date',
        'id',
        'static_id',
        'status',
        'time',
        'home_id',
        'home_goals',
        'home_name',
        'away_id',
        'away_goals',
        'away_name',
        'ht_score',
        'ft_score'

    ];
}
