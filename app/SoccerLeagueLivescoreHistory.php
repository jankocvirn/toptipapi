<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerLeagueLivescoreHistory extends Model
{
    protected $fillable=[

        'id','country',
        'name',
        'cup',
        'sub_id',
        'soccer_leaguecol',
        'updated'

    ];

}
