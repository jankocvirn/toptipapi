<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentSummary extends Model
{

    protected $fillable = [

        'match_id',
        'match_static_id',

        'goal_home_player_id',
        'goal_home_player_name',
        'goal_home_player_minute',

        'goal_away_player_id',
        'goal_away_player_name',
        'goal_away_player_minute',

        'yc_home_player_id',
        'yc_home_player_name',
        'yc_home_player_minute',

        'yc_away_player_id',
        'yc_away_player_name',
        'yc_away_player_minute',

        'rc_home_player_id',
        'rc_home_player_name',
        'rc_home_player_minute',

        'rc_away_player_id',
        'rc_away_player_name',
        'rc_away_player_minute'


    ];

}
