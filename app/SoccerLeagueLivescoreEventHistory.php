<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerLeagueLivescoreEventHistory extends Model
{
    protected $fillable=[

        'parents_match_id',

        'assist',
        'assistid',

        'extra_min',
        'minute',
        'id',
        'player',
        'player_id',
        'result',
        'team',
        'type'

    ];
}
