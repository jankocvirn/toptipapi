<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentPlayerStatsAway extends Model
{
    protected $fillable = [

        'match_id',
        'match_static_id',
        'assists',
        'fouls_commited',
        'fouls_drawn',
        'goals',
        'comment_id',
        'name',
        'num',
        'offsides',
        'pen_miss',
        'pen_score',
        'pos',
        'posx',
        'posy',
        'redcards',
        'saves',
        'shots_on_goal',
        'shots_total',
        'yellowcards'


    ];
}
