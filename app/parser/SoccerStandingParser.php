<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\SoccerScore;
use App\SoccerStandings;

class SoccerStandingParser
{


    public function startParser($fixture_data)
    {


        $this->parseXMLFeed($fixture_data);

    }




    private function parseXMLFeed($result_data)
    {



        $sport = $result_data->attributes()->sport;
        $updated=$result_data->attributes()->updated;
        $country=$result_data->attributes()->country;

        foreach ($result_data->league as $league){

            $league_name=$league->attributes()->name;
            $league_id=$league->attributes()->id;
            $league_sub_id=$league->attributes()->sub_id;
            $league_country=$league->attributes()->country;
            $league_season=$league->attributes()->season;
            $league_round=$league->attributes()->round;



            foreach($league->team as $team){


                $oObj=new SoccerStandings();
                $oObj->updated=$updated;
                $oObj->sport=$sport;
                $oObj->country=$country;
                $oObj->league_country=$league_country;
                $oObj->league_id=$league_id;
                $oObj->league_sub_id=$league_sub_id;
                $oObj->league_season=$league_season;
                $oObj->league_round=$league_round;
                $oObj->league_name=$league_name;


                //TEAM node
                $oObj->team_id=$team->attributes()->id;
                $oObj->team_name=$team->attributes()->name;
                $oObj->team_position=$team->attributes()->position;
                $oObj->recent_form=$team->attributes()->recent_form;
                $oObj->status=$team->attributes()->status;

                //Overall node

                $oObj->overall_draw=$team->overall->attributes()->draw;
                $oObj->overall_goal_attempted=$team->overall->attributes()->goals_attempted;
                $oObj->overall_scored=$team->overall->attributes()->goals_scored;
                $oObj->overall_lose=$team->overall->attributes()->lose;
                $oObj->overall_played=$team->overall->attributes()->played;
                $oObj->overall_win=$team->overall->attributes()->win;


                //home node
                $oObj->home_draw=$team->home->attributes()->draw;
                $oObj->home_goal_attempted=$team->home->attributes()->goals_attempted;
                $oObj->home_scored=$team->home->attributes()->goals_scored;
                $oObj->home_lose=$team->home->attributes()->lose;
                $oObj->home_played=$team->home->attributes()->played;
                $oObj->home_win=$team->home->attributes()->win;


                //away node
                $oObj->away_draw=$team->away->attributes()->draw;
                $oObj->away_goal_attempted=$team->away->attributes()->goals_attempted;
                $oObj->away_scored=$team->away->attributes()->goals_scored;
                $oObj->away_lose=$team->away->attributes()->lose;
                $oObj->away_played=$team->away->attributes()->played;
                $oObj->away_win=$team->away->attributes()->win;



                $oObj->totals_goal_diff=$team->totals->attributes()->goal_difference;
                $oObj->totals_points=$team->totals->attributes()->points;
                $oObj->special_name=$team->special->attributes()->name;


                $oObj->save();



            }



        }



    }


    private function checkArraySize($payload)
    {

        if (sizeof($payload) != 0) {

            return true;
        } else {

            return false;
        }

    }


}