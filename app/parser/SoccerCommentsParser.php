<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\Commentarie;
use App\CommentStatistics;
use App\CommentSummary;
use App\Fixture;
use App\LeagueFixture;
use App\MatchFixture;
use App\SoccerLeagueLivescore;
use App\SoccerLeagueLivescoreEvent;
use App\SoccerLivescoreMatch;
use Carbon\Carbon;
use Exception;
use Nathanmac\Utilities\Parser\Facades\Parser;
use SimpleXMLElement;

class SoccerCommentsParser
{


    public function startParser($fixture_data, $country)
    {


        $this->parseXMLFeed($fixture_data, $country);

    }


    /*
     * Parse the feed xml and store it in the database with details for
     * league , match , evnts.
     * **/


    private function parseXMLFeed($comment_data, $country)
    {


        $sport = $comment_data->attributes()->sport;


        foreach ($comment_data->league as $league) {


            $leagueid = $league->attributes()->id;
            $leaguesubid = $league->attributes()->sub_id;


            $leaguename = $league->attributes()->name;
            $leaguecountry = $league->attributes()->country;


            foreach ($league->match as $match) {


                $lObj = new Commentarie();
                $lObj->league_name = $leaguename;
                $lObj->country = $leaguecountry;
                $lObj->league_id = $leagueid;
                $lObj->league_sub_id = $leaguesubid;

                $lObj->country = $country;


                $lObj->status = $match->attributes()->status;
                $lObj->time = $match->attributes()->time;
                $lObj->static_id = $match->attributes()->static_id;
                $lObj->date = $match->attributes()->date;
                $lObj->alternate_id = $match->attributes()->alternate_id;
                $lObj->alternate_id_2 = $match->attributes()->alternate_id_2;
                $lObj->match_id = $match->attributes()->id;


                //Extract home and away
                $lObj->home_id = $match->home->attributes()->id;
                $lObj->home_name = $match->home->attributes()->name;
                $lObj->home_goals = $match->home->attributes()->goals;

                $lObj->away_id = $match->away->attributes()->id;
                $lObj->away_name = $match->away->attributes()->name;
                $lObj->away_goals = $match->away->attributes()->goals;


                //Extract MatchInfo


                $lObj->stadium = $match->matchinfo->stadium->attributes()->name;
                $lObj->attendance_name = $match->matchinfo->attendance->attributes()->attendance;
                $lObj->time_name = $match->matchinfo->time->attributes()->name;
                $lObj->referee = $match->matchinfo->referee->attributes()->name;

                $check_value=$match->stats;
                if ((string)$check_value==='') {
                }

                else{
                $sumObj=new CommentStatistics();

                $sumObj->match_id = $match->attributes()->id;
                $sumObj->match_static_id = $match->attributes()->static_id;


                   $sumObj->home_shots_ongoal = $match->stats->home->shots->attributes()->ongoal;
                    $sumObj->home_shots_total = $match->stats->home->shots->attributes()->total;
                    $sumObj->home_fouls_total = $match->stats->home->fouls->attributes()->total;
                    $sumObj->home_corners_total = $match->stats->home->corners->attributes()->total;
                    $sumObj->home_offsides_total = $match->stats->home->offsides->attributes()->total;
                    $sumObj->home_poss_time_total = $match->stats->home->possesiontime->attributes()->total;
                    $sumObj->home_yc_total = $match->stats->home->yellowcards->attributes()->total;
                    $sumObj->home_rc_total = $match->stats->home->redcards->attributes()->total;
                    $sumObj->home_saves_total = $match->stats->home->saves->attributes()->total;

                    //Extract Stats away team
                    $sumObj->away_shots_ongoal = $match->stats->away->shots->attributes()->ongoal;
                    $sumObj->away_shots_total = $match->stats->away->shots->attributes()->total;
                    $sumObj->away_fouls_total = $match->stats->away->fouls->attributes()->total;
                    $sumObj->away_corners_total = $match->stats->away->corners->attributes()->total;
                    $sumObj->away_offsides_total = $match->stats->away->offsides->attributes()->total;
                    $sumObj->away_poss_time_total = $match->stats->away->possesiontime->attributes()->total;
                    $sumObj->away_yc_total = $match->stats->away->yellowcards->attributes()->total;
                    $sumObj->away_rc_total = $match->stats->away->redcards->attributes()->total;
                    $sumObj->away_saves_total = $match->stats->away->saves->attributes()->total;

                    $sumObj->save();
                }

                $lObj->save();

            }


        }


    }


    private function checkArraySize($payload)
    {

        if (sizeof($payload) != 0) {

            return true;
        } else {

            return false;
        }

    }


}