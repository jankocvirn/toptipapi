<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\HighlightDetailsSoccer;
use App\HighlightsSoccer;

class SoccerHighlightsParser
{


    public function startParser($fixture_data)
    {


        $this->parseXMLFeed($fixture_data);

    }




    private function parseXMLFeed($result_data)
    {



        $sport = $result_data->attributes()->sport;
        $updated=$result_data->attributes()->updated;

        foreach ($result_data->league as $league){

            $league_name=$league->attributes()->name;
            $league_id=$league->attributes()->id;
            $league_sub_id=$league->attributes()->sub_id;



            foreach($league->match as $match){

                $mObj=new HighlightsSoccer();
                $mObj->sport=$sport;
                $mObj->league_name=$league_name;
                $mObj->league_id=$league_id;
                $mObj->league_sub_id=$league_sub_id;
                $mObj->maid=$match->attributes()->alternate_id;
                $mObj->maid2=$match->attributes()->alternate_id_2;
                $mObj->date=$match->attributes()->date;
                $match_id=$match->attributes()->id;
                $mObj->match_id=$match_id;
                $mObj->static_id=$match->attributes()->static_id;
                $mObj->status=$match->attributes()->status;
                $mObj->time=$match->attributes()->time;

                $mObj->home_goals=$match->home->attributes()->goals;
                $mObj->home_id=$match->home->attributes()->id;
                $mObj->home_name=$match->home->attributes()->name;

                $mObj->away_goals=$match->away->attributes()->goals;
                $mObj->away_id=$match->away->attributes()->id;
                $mObj->away_name=$match->away->attributes()->name;

                $mObj->save();

                foreach($match->match_highlights as $mh){

                    $hObj=new HighlightDetailsSoccer();
                    $hObj->match_id=$match_id;
                    $hObj->league_id=$league_id;

                    $hObj->link=$mh->match_highlight;

                    $hObj->save();

                }



            }



        }



    }


    private function checkArraySize($payload)
    {

        if (sizeof($payload) != 0) {

            return true;
        } else {

            return false;
        }

    }


}