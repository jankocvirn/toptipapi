<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\SoccerMatchOds;
use App\SoccerMatchOdsDetails;

class SoccerOddsParser
{


    public function startParser($fixture_data)
    {


        $this->parseXMLFeed($fixture_data);

    }




    private function parseXMLFeed($result_data)
    {



        $sport = $result_data->attributes()->sport;
        $updated=$result_data->attributes()->updated;

        foreach($result_data->league as $league){

            $league_country=$league->attributes()->country;
            $league_name=$league->attributes()->name;
            $league_id=$league->attributes()->id;
            $league_sub_id=$league->attributes()->sub_id;
            $league_cup=$league->attributes()->cup;

            foreach($league->match as $match){

                $mObj=new SoccerMatchOds();
                $mObj->country=$league_country;
                $mObj->cup=$league_cup;
                $mObj->lname=$league_name;
                $mObj->lid=$league_id;
                $mObj->lsub_id=$league_sub_id;
                $mObj->aid=$match->attributes()->alternate_id;
                $mObj->aid2=$match->attributes()->alternate_id_2;
                $mObj->match_id=$match->attributes()->id;
                $mObj->mstatic_id=$match->attributes()->static_id;
                $mObj->date=$match->attributes()->date;
                $mObj->status=$match->attributes()->status;
                $mObj->time=$match->attributes()->time;


                $mObj->home_aid=$match->home->attributes()->alternate_id;
                $match_id=$match->home->attributes()->id;
                $mObj->home_id=$match_id;
                $mObj->home_name=$match->home->attributes()->name;
                $mObj->away_aid=$match->away->attributes()->alternate_id;
                $mObj->away_id=$match->away->attributes()->id;
                $mObj->away_name=$match->away->attributes()->name;

                $mObj->save();

                foreach($match->odds as $odds){

                    foreach ($odds->type as $type){

                        $type_name=$type->attributes()->name;

                        foreach($type->bookmaker as $bookmaker){

                            $bookmaker_id=$bookmaker->attributes()->id;
                            $bookmaker_name=$bookmaker->attributes()->name;

                            foreach ($bookmaker->odd as $odd){

                                $oddObj=new SoccerMatchOdsDetails();
                                $oddObj->match_id=$match_id;
                                $oddObj->type_name=$type_name;
                                $oddObj->bookmark_id=$bookmaker_id;
                                $oddObj->bookmark_name=$bookmaker_name;
                                $oddObj->odd_name=$odd->attributes()->name;
                                $oddObj->odd_value=$odd->attributes()->value;

                                $oddObj->save();

                            }
                        }

                    }

                }

            }


        }






    }


    private function checkArraySize($payload)
    {

        if (sizeof($payload) != 0) {

            return true;
        } else {

            return false;
        }

    }


}