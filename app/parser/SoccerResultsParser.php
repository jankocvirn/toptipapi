<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\ResultLeague;
use App\ResultMatch;
use App\ResultMatchAwayPlayer;
use App\ResultMatchAwayPlayerSubs;
use App\ResultMatchGoals;
use App\ResultMatchHomePlayer;
use App\ResultMatchHomePlayerSubs;
use App\ResultStage;

class SoccerResultsParser
{


    public function startParser($fixture_data, $country)
    {


        $this->parseXMLFeed($fixture_data, $country);

    }


    /*
     * Parse the feed xml and store it in the database with details for
     * league , match , evnts.
     * **/


    private function parseXMLFeed($result_data, $country)
    {



        $sport = $result_data->attributes()->sport;
        $updated=$result_data->attributes()->updated;
        $country=$result_data->attributes()->country;


        foreach ($result_data->league as $league) {

            $lObj =new ResultLeague();

            $leagueid = $league->attributes()->id;
            $leaguesubid = $league->attributes()->sub_id;

            $season=$league->attributes()->season;
            $leaguename = $league->attributes()->name;


            $lObj->league_id=$leagueid;
            $lObj->name=$leaguename;
            $lObj->season=$season;
            $lObj->country=$country;
            $lObj->sport=$sport;
            $lObj->uploaded=$updated;
            $lObj->sub_id=$leaguesubid;

            $lObj->save();


            foreach($league->stage as $stage){

                $sObj=new ResultStage();
                $sObj->league_id=$leagueid;
                $sObj->name=$stage->attributes()->name;
                $sObj->round=$stage->attributes()->round;
                $stage_id=$stage->attributes()->stage_id;
                $sObj->stage_id=$stage_id;

                $sObj->save();





                    foreach($stage->week as $week){

                        $week_nr=$week->attributes()->number;

                        foreach($week->match as $match){

                            $mObj=new ResultMatch();
                            $mObj->stage_id=$stage_id;
                            $mObj->league_id=$leagueid;
                            $mObj->alternate_id=$match->attributes()->alternate_id;
                            $mObj->alternate_id_2=$match->attributes()->alternate_id_2;
                            $mObj->date=$match->attributes()->date;
                            $matchid=$match->attributes()->id;
                            $mObj->match_id=$matchid;
                            $matchstaticid=$match->attributes()->static_id;
                            $mObj->static_id=$matchstaticid;
                            $mObj->status=$match->attributes()->status;
                            $mObj->time=$match->attributes()->time;
                            $mObj->venue=$match->attributes()->venue;
                            $mObj->venue_city=$match->attributes()->venue_city;
                            $mObj->venue_id=$match->attributes()->venue_id;
                            //HOME
                            $mObj->het_score=$match->home->attributes()->et_score;
                            $mObj->hft_score=$match->home->attributes()->ft_score;
                            $mObj->home_id=$match->home->attributes()->id;
                            $mObj->home_name=$match->home->attributes()->name;
                            $mObj->home_pen_score=$match->home->attributes()->pen_score;
                            $mObj->home_score=$match->home->attributes()->score;

                            //AWAY

                            $mObj->aet_score=$match->away->attributes()->et_score;
                            $mObj->aft_score=$match->away->attributes()->ft_score;
                            $mObj->away_id=$match->away->attributes()->id;
                            $mObj->away_name=$match->away->attributes()->name;
                            $mObj->away_pen_score=$match->away->attributes()->pen_score;
                            $mObj->away_score=$match->away->attributes()->score;
                            
                            

                            //HALFTIME

                            $mObj->half_time_score=$match->halftime->attributes()->score;


                            $mObj->save();

                            foreach($match->goals->goal as $goal){

                                $gObj=new ResultMatchGoals();
                                $gObj->match_id=$matchid;
                                $gObj->league_id=$leagueid;
                                $gObj->league_sub_id=$leaguesubid;
                                $gObj->match_static_id=$matchstaticid;
                                $gObj->assist=$goal->attributes()->assist;
                                $gObj->minute=$goal->attributes()->minute;
                                $gObj->player=$goal->attributes()->player;
                                $gObj->playerid=$goal->attributes()->playerid;
                                $gObj->score=$goal->attributes()->score;
                                $gObj->team=$goal->attributes()->team;



                                $gObj->save();


                            }

                            foreach($match->lineups->home->player as $player){




                                $plObj=new ResultMatchHomePlayer();

                                $plObj->match_id=$matchid;
                                $plObj->league_id=$leagueid;
                                $plObj->league_sub_id=$leaguesubid;
                                $plObj->match_static_id=$matchstaticid;
                                $plObj->booking=$player->attributes()->booking;
                                $plObj->player_id=$player->attributes()->id;
                                $plObj->name=$player->attributes()->name;
                                $plObj->number=$player->attributes()->number;
                                $plObj->save();

                            }

                            foreach($match->lineups->away->player as $player){




                                $plObj=new ResultMatchAwayPlayer();

                                $plObj->match_id=$matchid;
                                $plObj->league_id=$leagueid;
                                $plObj->league_sub_id=$leaguesubid;
                                $plObj->match_static_id=$matchstaticid;
                                $plObj->booking=$player->attributes()->booking;
                                $plObj->player_id=$player->attributes()->id;
                                $plObj->name=$player->attributes()->name;
                                $plObj->number=$player->attributes()->number;
                                $plObj->save();

                            }



                            foreach($match->substitutions->home->substitution as $substitution){

                                $shObj=new ResultMatchHomePlayerSubs();


                                $shObj->league_id=$leagueid;
                                $shObj->league_sub_id=$leaguesubid;
                                $shObj->match_static_id=$matchstaticid;
                                $shObj->match_id=$matchid;
                                $shObj->minute=$substitution->attributes()->minute;
                                $shObj->player_in_booking=$substitution->attributes()->player_in_booking;
                                $shObj->player_in_name=$substitution->attributes()->player_in_name;
                                $shObj->player_in_id=$substitution->attributes()->player_in_id;
                                $shObj->player_in_number=$substitution->attributes()->player_in_number;
                                $shObj->player_out_id=$substitution->attributes()->player_out_id;
                                $shObj->player_out_name=$substitution->attributes()->player_out_name;
                                $shObj->save();

                            }

                            foreach($match->substitutions->away->substitution as $substitution){

                                $shObj=new ResultMatchAwayPlayerSubs();


                                $shObj->league_id=$leagueid;
                                $shObj->league_sub_id=$leaguesubid;
                                $shObj->match_static_id=$matchstaticid;
                                $shObj->match_id=$matchid;
                                $shObj->minute=$substitution->attributes()->minute;
                                $shObj->player_in_booking=$substitution->attributes()->player_in_booking;
                                $shObj->player_in_name=$substitution->attributes()->player_in_name;
                                $shObj->player_in_id=$substitution->attributes()->player_in_id;
                                $shObj->player_in_number=$substitution->attributes()->player_in_number;
                                $shObj->player_out_id=$substitution->attributes()->player_out_id;
                                $shObj->player_out_name=$substitution->attributes()->player_out_name;
                                $shObj->save();

                            }

                        }


                    }




            }


        }


    }


    private function checkArraySize($payload)
    {

        if (sizeof($payload) != 0) {

            return true;
        } else {

            return false;
        }

    }


}