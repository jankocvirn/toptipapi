<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\SoccerLeagueLivescore;
use App\SoccerLeagueLivescoreEvent;
use App\SoccerLivescoreMatch;
use Carbon\Carbon;
use Exception;
use Nathanmac\Utilities\Parser\Facades\Parser;
use SimpleXMLElement;

class SoccerParser
{




    /*
     * Parse the feed xml and store it in the database with details for
     * league , match , evnts.
     * **/


    public function parseXMLFeed($livescore_data){

        //$number_of_leagues=sizeof($livescore_data);

        $this->dropData();

        foreach ($livescore_data->league as $league) {

            // Begin for individual league


            //  League properties
            $league_name=$league->attributes()->name ;
            $league_country=$league->attributes()->country;
            $league_cup=$league->attributes()->cup;
            $league_id=$league->attributes()->id;
            $league_sub_id=$league->attributes()->sub_id;


            $leagueObj=new SoccerLeagueLivescore();
            $leagueObj->name=$league_name;
            $leagueObj->country=$league_country;
            $leagueObj->cup=$league_cup;
            $leagueObj->id=$league_id;
            $leagueObj->sub_id=$league_sub_id;
            $leagueObj->updated=Carbon::now();

            $leagueObj->save();



            // Iterate over matches in league to print teams and scores

            foreach ($league->match as $match) {

                // Set match variables
                $matchObj = new SoccerLivescoreMatch();

                $alternate_id = $match->attributes()->alternate_id;
                $alternate_id_2 = $match->attributes()->alternate_id_2;
                $commentary = $match->attributes()->commentary;
                $date = $match->attributes()->date;
                $time = $match->attributes()->time;
                $matchid = $match->attributes()->id;
                $static_id = $match->attributes()->static_id;
                $status = $match->attributes()->status;

                //set the league parent id from the feed:
                $matchObj->league_parent_id = $league_id;
                $matchObj->alternate_id = $alternate_id;
                $matchObj->alternate_id_2 = $alternate_id_2;
                $matchObj->date = $date;
                $matchObj->time = $time;
                $matchObj->id = $matchid;
                $matchObj->status = $status;
                $matchObj->static_id = $static_id;




                //Home and Away
                $home = $match->home->attributes()->name;
                $home_id = $match->home->attributes()->id;
                $score_home = $match->home->attributes()->goals;

                $matchObj->home_name = $home;
                $matchObj->home_id = $home_id;
                $matchObj->home_goals = $score_home;


                $away = $match->away->attributes()->name;
                $away_id = $match->away->attributes()->id;
                $score_away = $match->away->attributes()->goals;

                $matchObj->away_name = $away;
                $matchObj->away_id = $away_id;
                $matchObj->away_goals = $score_away;


                //HT and FT scores
                try {
                    $ht_score = $match->ht->attributes()->score;
                } catch (Exception $e) {
                    $ht_score = '/';
                }


                try {
                    $ft_score = $match->ft->attributes()->score;
                } catch (Exception $e) {
                    $ft_score = '/';
                }

                $matchObj->ht_score = $ht_score;
                $matchObj->ft_score = $ft_score;


                //Events
                $matchObj->save();



                $events_array = $match->events;

                if ($this->checkArraySize($events_array)) {


                    foreach ($match->events as $events) {



                        foreach ($events->event as $event) {

                            try {
                                $assist = $event->attributes()->assist;
                            } catch (Exception $e) {
                                $assist = null;
                            }
                            try {
                                $assistid = $event->attributes()->assistid;
                            } catch (Exception $e) {
                                $assist = null;
                            }
                            try {
                                $extra_min = $event->attributes()->extra_min;
                            } catch (Exception $e) {

                                $extra_min = null;
                            }

                            try {
                                $eventid = $event->attributes()->id;

                            }
                            catch(Exception $e){
                                $eventid=null;

                            }

                            try {
                                $minute = $event->attributes()->minute;
                            }
                            catch(Exception $e){
                                $minute=0;
                                }
                            try{
                            $player = $event->attributes()->player;
                            }
                            catch(Exception $e){
                            $player=0;
                            }
                            try{
                            $playerid = $event->attributes()->playerid;
                            }
                            catch(Exception $e){
                            $playerid=0;
                            }
                            try{
                            $result = $event->attributes()->result;
                            }
                            catch(Exception $e){
                            $result=0;
                            }
                            try{
                            $team = $event->attributes()->team;
                            }
                            catch(Exception $e){
                            $team=0;
                            }
                            try{
                            $type = $event->attributes()->type;
                            }
                            catch(Exception $e){
                            $type=0;
                            }



                            //set parent match id from the xml feed
                            $eventObj = new SoccerLeagueLivescoreEvent();
                            $eventObj->parents_match_id = $matchid;
                            $eventObj->assist = $assist;
                            $eventObj->assistid = $assistid;
                            $eventObj->extra_min = $extra_min;
                            $eventObj->id = $eventid;
                            $eventObj->minute = $minute;
                            $eventObj->player = $player;
                            $eventObj->player_id = $playerid;
                            $eventObj->result = $result;
                            $eventObj->team = $team;
                            $eventObj->type = $type;

                            $eventObj->save();


                        }
                    }


                }

            }
        }

    }

    /*
     * Truncate the tables and reset row id's
     * */

    private function dropData(){

        SoccerLeagueLivescore::truncate();
        SoccerLivescoreMatch::truncate();
        SoccerLeagueLivescoreEvent::truncate();

    }

    private function checkArraySize($payload){

        if (sizeof($payload)!=0){

            return true;
        }
        else{

            return false;
        }

    }



}