<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\Fixture;
use App\LeagueFixture;
use App\MatchFixture;
use App\SimpleLeagueFixture;
use App\SimpleMatchFixture;
use App\SoccerLeagueLivescore;
use App\SoccerLeagueLivescoreEvent;
use App\SoccerLivescoreMatch;
use Carbon\Carbon;
use Exception;
use Nathanmac\Utilities\Parser\Facades\Parser;
use SimpleXMLElement;

class SimpleFixturesParser
{


    public function startParser($fixture_data,$country){


        $this->parseXMLFeed($fixture_data,$country);

    }


    /*
     * Parse the feed xml and store it in the database with details for
     * league , match , evnts.
     * **/


    private function parseXMLFeed($fixture_data,$country){






        foreach ($fixture_data->league as $league){


            $leagueid=$league->attributes()->id ;
            $leaguesubid=$league->attributes()->sub_id ;

            $lObj=new SimpleLeagueFixture();
            $lObj->name=$league->attributes()->name ;
            $lObj->country=$league->attributes()->country ;
            $lObj->leagueid=$leagueid;
            $lObj->sub_id=$leaguesubid ;

            $lObj->country=$country;



            $lObj->save();



                foreach ($league->match as $match ) {

                    $mObj=new SimpleMatchFixture();

                    $mObj->status=$match->attributes()->status;
                    $mObj->time=$match->attributes()->time;
                    $mObj->static_id=$match->attributes()->static_id;
                    $mObj->date=$match->attributes()->date;
                    $mObj->alternate_id=$match->attributes()->alternate_id;
                    $mObj->alternate_id_2=$match->attributes()->alternate_id_2;
                    $mObj->matchid=$match->attributes()->id;
                    $mObj->league_id=$leagueid;
                    $mObj->league_sub_id=$leaguesubid;


                    //Extract subnodes
                    $mObj->homename=$match->home->attributes()->name;
                    $mObj->homeid=$match->home->attributes()->id;


                    $mObj->awayname=$match->away->attributes()->name;
                    $mObj->awayid=$match->away->attributes()->id;





                    $mObj->save();

                }


            }


        }

        







    private function checkArraySize($payload){

        if (sizeof($payload)!=0){

            return true;
        }
        else{

            return false;
        }

    }



}