<?php
/**
 * Created by PhpStorm.
 * User: janko
 * Date: 20.11.15.
 * Time: 12:50
 */

namespace app\parser;


use App\SoccerScore;

class SoccerScoresParser
{


    public function startParser($fixture_data)
    {


        $this->parseXMLFeed($fixture_data);

    }




    private function parseXMLFeed($result_data)
    {



        $sport = $result_data->attributes()->sport;
        $updated=$result_data->attributes()->updated;
        $country=$result_data->attributes()->country;

        foreach ($result_data->league as $league){

            $league_name=$league->attributes()->name;
            $league_id=$league->attributes()->id;
            $league_sub_id=$league->attributes()->sub_id;
            $league_country=$league->attributes()->country;



            foreach($league->player as $player){


                $oObj=new SoccerScore();
                $oObj->feed_updated=$updated;
                $oObj->sport=$sport;
                $oObj->country=$country;
                $oObj->league_country=$league_country;
                $oObj->league_id=$league_id;
                $oObj->league_sub_id=$league_sub_id;
                $oObj->player_goals=$player->attributes()->goals;
                $oObj->player_id=$player->attributes()->id;
                $oObj->name=$player->attributes()->name;
                $oObj->penalty_goals=$player->attributes()->penalty_goals;
                $oObj->pos=$player->attributes()->pos;
                $oObj->team=$player->attributes()->team;
                $oObj->team_id=$player->attributes()->team_id;
                $oObj->save();



            }



        }



    }


    private function checkArraySize($payload)
    {

        if (sizeof($payload) != 0) {

            return true;
        } else {

            return false;
        }

    }


}