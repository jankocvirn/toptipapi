<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentHomeTeam extends Model
{
    protected $fillable = [


        'match_id',
        'match_static_id',

        'player_id',
        'number',
        'name',
        'pos'


    ];
}
