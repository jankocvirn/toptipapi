<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchFixture extends Model
{
    protected $fillable=[

        'status','time','static_id','date',
        'alternate_id','alternate_id_2','matchid','league_id','league_sub_id',
        'homename','homeid','awayname','awayid','venue',
        'venue_city',
        'venue_id',
        'et_score_home',
        'ft_score_home',
        'pen_score_home',
        'score_home',
        'et_score_away',
        'ft_score_away',
        'pen_score_away',
        'score_away'

    ];
}
