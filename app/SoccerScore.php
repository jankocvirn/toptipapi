<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerScore extends Model
{
    protected $fillable = [

        'feed_updated',
        'sport',
        'country',
        'league_country',
        'league_id',
        'league_sub_id',
        'player_goals',
        'player_id',
        'name',
        'penalty_goals',
        'pos',
        'team',
        'team_id'


    ];
}
