<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoccerMatchOds extends Model
{
    protected $fillable = [

        'country',
        'cup',
        'lid',
        'lname',
        'lsub_id',
        'aid',
        'aid2',
        'match_id',
        'mstatic_id',
        'home_aid',
        'home_id',
        'away_aid',
        'away_id',
        'date',
        'status',
        'time',
        'home_name',
        'away_name'


    ];
}
