<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentHomeSubstitution extends Model
{
    protected $fillable = [

        'match_id',
        'match_static_id',
        'minute',
        'off',
        'off_id',
        'on',
        'on_id'


    ];
}
