<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeagueFixture extends Model
{
    protected $fillable=[

        'country','cup','leagueid','sub_id',
        'name','season'

    ];
}
