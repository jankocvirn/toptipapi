<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighlightsSoccer extends Model
{
    protected $fillable = [

        'sport',
        'league_name',
        'league_id',
        'league_sub_id',
        'maid',
        'maid2',
        'date',
        'match_id',
        'static_id',
        'status',
        'time',
        'home_goals',
        'home_id',
        'home_name',
        'away_goals',
        'away_id',
        'away_name'


    ];
}
