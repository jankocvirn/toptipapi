<?php

namespace App\Console\Commands;

use App\clients\TipGinClientScores;
use Illuminate\Console\Command;

class SoccerScores extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('SoccerScores download started!');
        $client=new TipGinClientScores();
        $client->startJob();
        $this->info('SoccerScores download finished!');
    }
}
