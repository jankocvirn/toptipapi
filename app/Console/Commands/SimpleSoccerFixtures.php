<?php

namespace App\Console\Commands;

use App\clients\TipGinClientSimpleFixtures;
use Illuminate\Console\Command;

class SimpleSoccerFixtures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'simplefixtures';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('Simple Fixtures download started!');
        $client=new TipGinClientSimpleFixtures();
        $client->startJob();
        $this->info('Simple Fixtures download finished!');
    }
}
