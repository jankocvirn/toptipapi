<?php

namespace App\Console\Commands;

use App\clients\TipGinClientStandings;
use Illuminate\Console\Command;

class SoccerStandings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'standings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('Soccer Standings download started!');
        $client=new TipGinClientStandings();
        $client->startJob();
        $this->info('Soccer Standings download finished!');
    }

}
