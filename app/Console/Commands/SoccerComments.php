<?php

namespace App\Console\Commands;

use App\clients\TipGinClientComments;
use Illuminate\Console\Command;

class SoccerComments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'soccer_comments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('SoccerComments download started!');
        $client=new TipGinClientComments();
        $client->startJob();
        $this->info('SoccerComments download finished!');
    }
}
