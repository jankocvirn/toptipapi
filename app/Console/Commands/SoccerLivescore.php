<?php

namespace App\Console\Commands;

use App\clients\TipGinClient;
use Illuminate\Console\Command;

class SoccerLivescore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:get_soccer_livescore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the soccer livescores feed from the TipGin xml feeds server.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    public function fire(){

        $client=new TipGinClient();
        $client->startJob();

    }
}
