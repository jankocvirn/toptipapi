<?php

namespace App\Console\Commands;

use App\clients\TipGinClientOdds;
use Illuminate\Console\Command;

class SoccerOdds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'odds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('Soccer Odds download started!');
        $client=new TipGinClientOdds();
        $client->startJob();
        $this->info('Soccer Odds download finished!');
    }
}
