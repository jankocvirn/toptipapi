<?php

namespace App\Console\Commands;

use App\clients\TipGinClientResults;
use Illuminate\Console\Command;

class SoccerResults extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'results';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('SoccerResults download started!');
        $client=new TipGinClientResults();
        $client->startJob();
        $this->info('SoccerResults download finished!');
    }
}
