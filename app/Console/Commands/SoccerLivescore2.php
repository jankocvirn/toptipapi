<?php

namespace App\Console\Commands;

use App\clients\TipGinClient;
use Illuminate\Console\Command;

class SoccerLivescore2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'livescore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire(){

        $client=new TipGinClient();
        $client->startJob();

    }
}
