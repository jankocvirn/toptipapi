<?php

namespace App\Console\Commands;

use App\clients\TipGinClientHighlight;
use Illuminate\Console\Command;

class SoccerHighlights extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'highlights';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $this->info('SoccerHighlights download started!');
        $client=new TipGinClientHighlight();
        $client->startJob();
        $this->info('SoccerHighlights download finished!');
    }

}
