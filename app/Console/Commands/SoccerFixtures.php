<?php

namespace App\Console\Commands;

use App\clients\TipGinClientFixtures;
use Illuminate\Console\Command;

class SoccerFixtures extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixtures';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch extended fixtures from tipgin server.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info('Extended Fixtures download started!');
        $client=new TipGinClientFixtures();
        $client->startJob();
        $this->info('Extended Fixtures download finished!');
    }
}
