<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\SoccerLivescore2::class,
        \App\Console\Commands\SoccerFixtures::class,
        \App\Console\Commands\SimpleSoccerFixtures::class,
        \App\Console\Commands\SoccerComments::class,
        \App\Console\Commands\SoccerResults::class,
        \App\Console\Commands\SoccerHighlights::class,
        \App\Console\Commands\SoccerScores::class,
        \App\Console\Commands\SoccerStandings::class,
        \App\Console\Commands\SoccerOdds::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
        $schedule->command('livescore')->everyMinute();
        $schedule->command('fixtures')->daily();
        $schedule->command('simplefixtures')->daily();
        $schedule->command('soccer_comments')->everyFiveMinutes();
        $schedule->command('results')->everyTenMinutes();
        $schedule->command('highlights')->daily();
        $schedule->command('scores')->everyTenMinutes();
        $schedule->command('standings')->everyTenMinutes();
        $schedule->command('odds')->everyFiveMinutes();
    }
}
