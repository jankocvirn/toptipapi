<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_fixtures', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('status');
            $table->string('time');
            $table->string('static_id');
            $table->string('date');
            $table->bigInteger('alternate_id');
            $table->bigInteger('alternate_id_2');
            $table->bigInteger('matchid');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->string('homename');
            $table->bigInteger('homeid');
            $table->string('awayname');
            $table->bigInteger('awayid');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_fixtures');
    }
}
