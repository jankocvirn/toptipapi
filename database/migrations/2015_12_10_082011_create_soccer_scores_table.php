<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('feed_updated');
            $table->string('sport');
            $table->string('country');
            $table->string('league_country');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->integer('player_goals');
            $table->bigInteger('player_id');
            $table->string('name');
            $table->integer('penalty_goals');
            $table->integer('pos');
            $table->string('team');
            $table->integer('team_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_scores');
    }
}
