<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_standings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('updated');
            $table->string('sport');
            $table->string('country');
            $table->string('league_country');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->string('league_name');
            $table->string('league_season');
            $table->string('league_round');
            $table->bigInteger('team_id');
            $table->string('team_name');
            $table->integer('team_position')->nullable();
            $table->string('recent_form')->nullable();
            $table->string('status');

            $table->integer('overall_draw')->nullable();
            $table->integer('overall_goal_attempted')->nullable();
            $table->integer('overall_scored')->nullable();
            $table->integer('overall_lose')->nullable();
            $table->integer('overall_played')->nullable();
            $table->integer('overall_win')->nullable();

            $table->integer('home_draw')->nullable();
            $table->integer('home_goal_attempted')->nullable();
            $table->integer('home_scored')->nullable();
            $table->integer('home_lose')->nullable();
            $table->integer('home_played')->nullable();
            $table->integer('home_win')->nullable();

            $table->integer('away_draw')->nullable();
            $table->integer('away_goal_attempted')->nullable();
            $table->integer('away_scored')->nullable();
            $table->integer('away_lose')->nullable();
            $table->integer('away_played')->nullable();
            $table->integer('away_win')->nullable();

            $table->string('totals_goal_diff')->nullable();
            $table->string('totals_points')->nullable();
            $table->string('special_name')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_standings');
    }
}
