<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultLeaguesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_leagues', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('league_id');
            $table->string('name');
            $table->string('season');
            $table->bigInteger('sub_id');
            $table->string('country');
            $table->string('uploaded');
            $table->string('sport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('result_leagues');
    }
}
