<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_matches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('stage_id');
            $table->bigInteger('league_id');
            $table->bigInteger('alternate_id')->nullable();
            $table->bigInteger('alternate_id_2');
            $table->string('date');
            $table->bigInteger('match_id');
            $table->bigInteger('static_id');
            $table->string('status');
            $table->string('time');
            $table->string('venue');
            $table->string('venue_city');
            $table->bigInteger('venue_id');
            $table->integer('het_score')->nullable();
            $table->integer('hft_score')->nullable();
            $table->integer('home_id')->nullable();
            $table->string('home_name')->nullable();
            $table->integer('home_pen_score')->nullable();
            $table->integer('home_score')->nullable();
            $table->integer('aet_score')->nullable();
            $table->integer('aft_score')->nullable();
            $table->integer('away_id')->nullable();
            $table->string('away_name')->nullable();
            $table->integer('away_pen_score')->nullable();
            $table->integer('away_score')->nullable();
            $table->string('half_time_score')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('result_matches');
    }
}
