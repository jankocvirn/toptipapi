<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerMatchOdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_match_ods', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('country');
            $table->string('cup');
            $table->bigInteger('lid');
            $table->string('lname');
            $table->bigInteger('lsub_id');

            $table->bigInteger('aid')->nullable();
            $table->bigInteger('aid2')->nullable();
            $table->bigInteger('match_id')->nullable();
            $table->bigInteger('mstatic_id')->nullable();
            $table->bigInteger('home_aid')->nullable();
            $table->bigInteger('home_id')->nullable();
            $table->bigInteger('away_aid')->nullable();
            $table->bigInteger('away_id')->nullable();

            $table->string('date')->nullable();
            $table->string('status')->nullable();
            $table->string('time')->nullable();
            $table->string('home_name')->nullable();
            $table->string('away_name')->nullable();





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_match_ods');
    }
}
