<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerMatchOdsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_match_ods_details', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id')->nullable();
            $table->string('type_name')->nullable();
            $table->bigInteger('bookmark_id')->nullable();
            $table->string('bookmark_name')->nullable();
            $table->string('odd_name')->nullable();
            $table->decimal('odd_value')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_match_ods_details');
    }
}
