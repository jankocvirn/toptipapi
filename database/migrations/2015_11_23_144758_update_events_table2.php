<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soccer_league_livescore_events', function (Blueprint $table) {
            // change() tells the Schema builder that we are altering a table

            $table->string('assist')->nullable()->change();
            $table->integer('assistid')->unsigned()->nullable()->change();

            $table->string('extra_min')->nullable()->change();
            $table->string('minute')->nullable()->change();;
            $table->integer('id')->unsigned()->nullable()->change();;
            $table->string('player')->nullable()->change();;
            $table->integer('player_id')->unsigned()->nullable()->change();;
            $table->string('result')->nullable()->change();;
            $table->string('team')->nullable()->change();;
            $table->string('type')->nullable()->change();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
