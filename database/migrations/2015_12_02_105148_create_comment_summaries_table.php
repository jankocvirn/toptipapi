<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('match_static_id');
            
            $table->bigInteger('goal_home_player_id')->nullable();
            $table->string('goal_home_player_name')->nullable();
            $table->string('goal_home_player_minute')->nullable();
            
            $table->bigInteger('goal_away_player_id')->nullable();
            $table->string('goal_away_player_name')->nullable();
            $table->string('goal_away_player_minute')->nullable();

            $table->bigInteger('yc_home_player_id')->nullable();
            $table->string('yc_home_player_name')->nullable();
            $table->string('yc_home_player_minute')->nullable();

            $table->bigInteger('yc_away_player_id')->nullable();
            $table->string('yc_away_player_name')->nullable();
            $table->string('yc_away_player_minute')->nullable();

            $table->bigInteger('rc_home_player_id')->nullable();
            $table->string('rc_home_player_name')->nullable();
            $table->string('rc_home_player_minute')->nullable();

            $table->bigInteger('rc_away_player_id')->nullable();
            $table->string('rc_away_player_name')->nullable();
            $table->string('rc_away_player_minute')->nullable();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_summaries');
    }
}
