<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultMatchGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_match_goals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->bigInteger('match_static_id');
            $table->string('assist');
            $table->string('minute');
            $table->string('player');
            $table->string('playerid');
            $table->string('score');
            $table->string('team');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('result_match_goals');
    }
}
