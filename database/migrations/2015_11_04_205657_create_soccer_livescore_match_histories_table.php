<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerLivescoreMatchHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_livescore_match_histories', function (Blueprint $table) {

            $table->increments('idsoccer_livescore_match');
            $table->timestamps();
            $table->integer('league_parent_id');
            $table->integer('alternate_id');
            $table->integer('alternate_id_2');
            $table->string('date');
            $table->integer('id');
            $table->integer('static_id');
            $table->string('status');
            $table->string('time');
            $table->integer('home_id');
            $table->integer('home_goals');
            $table->string('home_name');
            $table->integer('away_id');
            $table->integer('away_goals');
            $table->string('away_name');
            $table->string('ht_score');
            $table->string('ft_score');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_livescore_match_histories');
    }
}
