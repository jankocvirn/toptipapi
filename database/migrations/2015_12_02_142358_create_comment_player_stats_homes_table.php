<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentPlayerStatsHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_player_stats_homes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('match_static_id');
            $table->string('assists')->nullable();
            $table->string('fouls_commited')->nullable();
            $table->string('fouls_drawn')->nullable();
            $table->string('goals')->nullable();
            $table->bigInteger('comment_id')->nullable();
            $table->string('name')->nullable();
            $table->bigInteger('num')->nullable();
            $table->string('offsides')->nullable();
            $table->string('pen_miss')->nullable();
            $table->string('pen_score')->nullable();
            $table->string('pos')->nullable();
            $table->string('posx')->nullable();
            $table->string('posy')->nullable();
            $table->string('redcards')->nullable();
            $table->string('saves')->nullable();
            $table->string('shots_on_goal')->nullable();
            $table->string('shots_total')->nullable();
            $table->string('yellowcards')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_player_stats_homes');
    }
}
