<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentCommentariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_commentaries', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('match_static_id');
            $table->string('comment');
            $table->string('comment_id');
            $table->string('important');
            $table->string('isgoal');
            $table->string('minute');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_commentaries');
    }
}
