<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighlightsSoccersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('highlights_soccers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('sport');
            $table->string('league_name');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->bigInteger('maid')->nullable();
            $table->bigInteger('maid2')->nullable();
            $table->string('date');
            $table->bigInteger('match_id');
            $table->bigInteger('static_id');
            $table->string('status');
            $table->string('time');
            $table->integer('home_goals')->nullable();
            $table->integer('home_id')->nullable();
            $table->integer('home_name')->nullable();
            $table->integer('away_goals')->nullable();
            $table->integer('away_id')->nullable();
            $table->integer('away_name')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('highlights_soccers');
    }
}
