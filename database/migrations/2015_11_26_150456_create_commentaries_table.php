<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentaries', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('sport');
            $table->string('country');
            $table->string('league_name');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->bigInteger('alternate_id')->nullable();
            $table->bigInteger('alternate_id_2')->nullable();
            $table->string('date');
            $table->bigInteger('match_id');
            $table->string('static_id');
            $table->string('status');
            $table->string('time');
            $table->string('home_goals');
            $table->string('home_id');
            $table->string('home_name');
            $table->string('away_goals');
            $table->string('away_id');
            $table->string('away_name');
            $table->string('stadium');
            $table->string('attendance_name');
            $table->string('time_name');
            $table->string('referee');
            $table->string('comment');
            $table->string('comment_id');
            $table->string('comment_important');
            $table->string('comment_isgoal');
            $table->string('comment_minute')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commentaries');
    }
}
