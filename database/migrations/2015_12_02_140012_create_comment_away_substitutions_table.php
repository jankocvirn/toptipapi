<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentAwaySubstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_away_substitutions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('match_static_id');
            $table->string('minute');
            $table->string('off');
            $table->string('off_id');
            $table->string('on');
            $table->string('on_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_away_substitutions');
    }
}
