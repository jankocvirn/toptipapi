<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('match_static_id');

            $table->string('home_shots_ongoal')->nullable();
            $table->string('home_shots_total')->nullable();
            $table->string('home_fouls_total')->nullable();
            $table->string('home_corners_total')->nullable();
            $table->string('home_offsides_total')->nullable();
            $table->string('home_poss_time_total')->nullable();
            $table->string('home_yc_total')->nullable();
            $table->string('home_rc_total')->nullable();
            $table->string('home_saves_total')->nullable();

            $table->string('away_shots_ongoal')->nullable();
            $table->string('away_shots_total')->nullable();
            $table->string('away_fouls_total')->nullable();
            $table->string('away_corners_total')->nullable();
            $table->string('away_offsides_total')->nullable();
            $table->string('away_poss_time_total')->nullable();
            $table->string('away_yc_total')->nullable();
            $table->string('away_rc_total')->nullable();
            $table->string('away_saves_total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_statistics');
    }
}
