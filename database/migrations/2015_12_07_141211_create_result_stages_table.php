<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('league_id');
            $table->string('name');
            $table->string('round');
            $table->bigInteger('stage_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('result_stages');
    }
}
