<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::query("ALTER TABLE soccer_league_livescore_events CHANGE assist assist VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
                   ALTER TABLE soccer_league_livescore_events CHANGE assistid assistid INT(11) NULL;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
