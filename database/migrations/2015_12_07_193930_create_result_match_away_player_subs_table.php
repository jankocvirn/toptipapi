<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultMatchAwayPlayerSubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_match_away_player_subs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('league_id');
            $table->bigInteger('league_sub_id');
            $table->bigInteger('match_static_id');
            $table->string('minute')->nullable();
            $table->string('player_in_booking')->nullable();
            $table->string('player_in_name')->nullable();
            $table->string('player_in_id')->nullable();
            $table->string('player_in_number')->nullable();
            $table->string('player_out_id')->nullable();
            $table->string('player_out_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('result_match_away_player_subs');
    }
}
