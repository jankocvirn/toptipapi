<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentAwayTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_away_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('match_id');
            $table->bigInteger('match_static_id');

            $table->bigInteger('player_id');
            $table->bigInteger('number');
            $table->string('name');
            $table->string('pos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_away_teams');
    }
}
