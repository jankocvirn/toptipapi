<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerLeagueLivescoreHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_league_livescore_histories', function (Blueprint $table) {
            $table->increments('idsoccer_league');
            $table->timestamps();
            $table->integer('id');
            $table->string('country');
            $table->string('name');
            $table->boolean('cup');
            $table->integer('sub_id');
            $table->string('soccer_leaguecol');
            $table->string('updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_league_livescore_histories');
    }
}
