<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class UpdateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commentaries', function ($table) {
            $table->string('home_goals')->nullable()->change();
            $table->string('home_id')->nullable()->change();
            $table->string('home_name')->nullable()->change();
            $table->string('away_goals')->nullable()->change();
            $table->string('away_id')->nullable()->change();
            $table->string('away_name')->nullable()->change();
            $table->string('stadium')->nullable()->change();
            $table->string('attendance_name')->nullable()->change();
            $table->string('time_name')->nullable()->change();
            $table->string('referee')->nullable()->change();
            $table->string('comment')->nullable()->change();
            $table->string('comment_id')->nullable()->change();
            $table->string('comment_important')->nullable()->change();
            $table->string('comment_isgoal')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
