<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMatchFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match_fixtures', function ($table) {
            $table->string('venue');
            $table->string('venue_city');
            $table->string('venue_id');
            $table->string('et_score_home')->nullable();
            $table->string('ft_score_home')->nullable();
            $table->string('pen_score_home')->nullable();
            $table->string('score_home')->nullable();
            $table->string('et_score_away')->nullable();
            $table->string('ft_score_away')->nullable();
            $table->string('pen_score_away')->nullable();
            $table->string('score_away')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
