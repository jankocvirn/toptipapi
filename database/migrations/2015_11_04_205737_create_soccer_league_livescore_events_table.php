<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoccerLeagueLivescoreEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soccer_league_livescore_events', function (Blueprint $table) {
            $table->increments('idsoccer_league_livescore_id');
            $table->timestamps();
            $table->integer('parents_match_id');

            $table->string('assist');
            $table->integer('assistid');

            $table->string('extra_min');
            $table->string('minute');
            $table->integer('id');
            $table->string('player');
            $table->integer('player_id');
            $table->string('result');
            $table->string('team');
            $table->string('type');




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('soccer_league_livescore_events');
    }
}
